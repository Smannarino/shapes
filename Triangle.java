public class Triangle{
    private double base;
    private double height;

    public Triangle(double base,double height){
        this.base=base;
        this.height=height;
    }
    public double getBase(){
        return this.base;
    }
    public double getHeight(){
        return this.height;
    }
    public double getArea(){
        return (this.base * this.height) / 2;  
    }
    public String toString(){
        return "your triangle has a base of "+this.base+" and a height of "+this.height;
    }
}