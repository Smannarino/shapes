public class Rectangle{
  
    private double length;
  private double width;
  
  public Rectangle(double len, double wid) throws IllegalArgumentException
  {
    if(len <= 0 || wid <= 0){
      throw new IllegalArgumentException("Parameter can not be less or equal to 0");
    }
    else{
    this.length=len;
    this.width = wid;
     }
  }
  public double getLength(){
    return length;
  }
  public double getWidth(){
    return width;
  } 
  
  public double getArea(){
    return width * length;
  }

  public double getPerimeter()
  {
    return 2*(width+length);
  }
  

  public String toString(){
    return "This Rectangle object's length is: "+length+", and width is: "+width;
  }
}