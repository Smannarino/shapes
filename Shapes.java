import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Shapes{
    public static void main(String[] args){
        Triangle tri=new Triangle(5.00, 8.00);

        Rectangle r = new Rectangle(3,5);
        System.out.println(tri);
       System.out.println(tri.getBase());

        System.out.println(tri.getBase());

        System.out.println("the area of the triangle is "+tri.getArea());
        System.out.println(r.toString());
        
    
    }

    @Test
    public void getAreaRectangleTest(){
        Rectangle r1 = new Rectangle(5, 3);
        assertEquals(15, r1.getArea());
    }

    @Test
    public void getLengthRectangleTest(){
        Rectangle r1 = new Rectangle(5, 3);
        assertEquals(5, r1.getLength());
    }

    @Test
    public void getPerimeterTest(){
        Rectangle r = new Rectangle(10, 20);
        assertEquals(60, r.getPerimeter());
    }

    @Test
    public void getLengthRectangleTest2(){
        Rectangle r1 = new Rectangle(-5, 3);
        assertEquals(-5, r1.getLength());
    }


}